from bottle import get,post,request,run,route,put
import pymysql

@route('/app')
def app():

	return '''<form name="Update and Search Artists" action="/Update_and_Search_Artists" method="post">
		  <input type="submit" value="Update & Search Artists"></form>
		  <form name="Search Songs" action="/Presentation_Of_Songs" method="post">
		  <input type="submit" value="Search Songs"></form>
		  <form name="Insert Artist" action="/Insert_Artists" method="post">
		  <input type="submit" value="Insert Artists"></form>
		  <form name="Insert Song" action="/Insert_Songs" method="post">
		  <input type="submit" value="Insert Songs"></form>'''

@route('/Update_and_Search_Artists',method='POST')
def Update_and_Search_Artists():

	return '''
		
		<h1><strong>Presentation Of Artists</strong>
		</h1><hr><br>

		<table><form name="search" action="/do_search" method="post">
			<tr><td>Name:</td><td><input type="text" name="name"></td></tr>
			<tr><td>Surame:</td><td><input type="text" name="surname"></td></tr>
			<tr><td>Birth Year-From:</td><td><input type="text" name="birthfrom"></td></tr>
			<tr><td>Birth Year-To:</td><td><input type="text" name="birthto"></td></tr>
			<tr><td></td><td><input type="radio" name="type" value="Singer"> Singer</td></tr>
			<tr><td>Type:</td><td><input type="radio" name="type" value="SongWriter"> SongWriter</td></tr>
			<tr><td></td><td><input type="radio" name="type" value="Composer" checked> Composer</td></tr>
			<tr><td></td><td><input type="submit" value="Submit"></td></tr>
			<br></form></table><hr>
		'''

@route('/do_search',method='POST')
def do_search():
	name=request.forms.get('name')
	surname=request.forms.get('surname')
	birthfrom=request.forms.get('birthfrom')
	birthto=request.forms.get('birthto')

	tp=request.forms.get('type')
	sql=''
	
	if tp == 'Singer':
		if((not name) and (not surname) and (not birthfrom) and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif((not name) and (not surname) and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.etos_gen >=\"'''+str(birthfrom)+'''\";'''
		elif((not name) and (not surname) and birthfrom and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND
				 k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif((not name) and surname and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.epitheto=\"'''+surname+'''\";'''
		elif(name and (not surname) and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.onoma=\"'''+name+'''\";'''
		elif((not name) and surname and (not birthfrom) and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.epitheto=\"'''+surname+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif((not name) and surname and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\";'''
		elif((not name) and surname and birthfrom and birthto):	
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\";'''			
		elif(name and (not surname) and (not birthfrom) and birthto):	
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.onoma=\"'''+name+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif(name and (not surname) and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.onoma=\"'''+name+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\";'''
		elif(name and (not surname) and birthfrom and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND (k.onoma=\"'''+name+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\");'''
		elif(name and surname and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\";'''
		elif(name and surname and (not birthfrom) and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND (k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\")'''
		elif(name and surname and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND (k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\");'''
		elif(name and surname and birthfrom and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND (k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND
				 k.etos_gen <=\"'''+str(birthto)+'''\");'''
		else:
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				     FROM kalitexnis k,singer_prod s
				     WHERE k.ar_taut=s.tragoudistis;'''
	elif tp == 'SongWriter':
		if((not name) and (not surname) and (not birthfrom) and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif((not name) and (not surname) and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.etos_gen >=\"'''+str(birthfrom)+'''\";'''
		elif((not name) and (not surname) and birthfrom and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND
				 k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif((not name) and surname and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.epitheto=\"'''+surname+'''\";'''
		elif(name and (not surname) and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.onoma=\"'''+name+'''\";'''
		elif((not name) and surname and (not birthfrom) and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.epitheto=\"'''+surname+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif((not name) and surname and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\";'''
		elif((not name) and surname and birthfrom and birthto):	
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\";'''			
		elif(name and (not surname) and (not birthfrom) and birthto):	
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.onoma=\"'''+name+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif(name and (not surname) and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.onoma=\"'''+name+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\";'''
		elif(name and (not surname) and birthfrom and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND (k.onoma=\"'''+name+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\");'''
		elif(name and surname and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\";'''
		elif(name and surname and (not birthfrom) and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND (k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\")'''
		elif(name and surname and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND (k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\");'''
		elif(name and surname and birthfrom and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND (k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND
				 k.etos_gen <=\"'''+str(birthto)+'''\");'''
		else:
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				     FROM kalitexnis k,tragoudi t
				     WHERE k.ar_taut=t.stixourgos;'''
	elif tp == 'Composer':
		if((not name) and (not surname) and (not birthfrom) and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif((not name) and (not surname) and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.etos_gen >=\"'''+str(birthfrom)+'''\";'''
		elif((not name) and (not surname) and birthfrom and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND
				 k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif((not name) and surname and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.epitheto=\"'''+surname+'''\";'''
		elif(name and (not surname) and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.onoma=\"'''+name+'''\";'''
		elif((not name) and surname and (not birthfrom) and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.epitheto=\"'''+surname+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif((not name) and surname and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\";'''
		elif((not name) and surname and birthfrom and birthto):	
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\";'''			
		elif(name and (not surname) and (not birthfrom) and birthto):	
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.onoma=\"'''+name+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif(name and (not surname) and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.onoma=\"'''+name+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\";'''
		elif(name and (not surname) and birthfrom and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND (k.onoma=\"'''+name+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\");'''
		elif(name and surname and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\";'''
		elif(name and surname and (not birthfrom) and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND (k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\")'''
		elif(name and surname and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND (k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\");'''
		elif(name and surname and birthfrom and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND (k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND
				 k.etos_gen <=\"'''+str(birthto)+'''\");'''
		elif((not name) and (not surname) and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				     FROM kalitexnis k,tragoudi t
				     WHERE k.ar_taut=t.sinthetis;'''

	
	db = pymysql.connect('localhost',port=3306,user='root',passwd='',db='songs',charset='utf8')
	cursor = db.cursor()
	
	cursor.execute(sql)

	s='''<table border=1>
  		<tr>
    		<th><b>National ID</b></th>
    		<th><b>Name</b></th>
    		<th><b>Surname</b></th>
		<th><b>Birth_Year</b></th>
		<th><b>Edit?</b></th>
 	</tr>'''
	
	for row in cursor:
				
		s1=unicode('''<tr><form name="edit" action="/edit_me" method="post">
				<td>'''+row[0]+'''</td>
				<td>'''+row[1]+'''</td>
				<td>'''+row[2]+'''</td>
				<td>'''+str(row[3])+'''</td>
				<td><input type="submit" value="Edit me!"></td>
				<td><input type=hidden name="NationalID" value="'''+row[0]+'''"></td></form></tr>''')
		s=s+s1

	s=s+'''</table>'''

	cursor.close()
	db.close()
	
	return s

@route('/edit_me',method='POST')
def edit_me():
	
	NationalID=request.forms.get('NationalID')

	return '''<h1><hr><strong>Update Artist Information</strong></h1><hr>
		  <table><form name="update" action="/update" method="post">
		  <tr><td>Name</td><td><input type="text" name="Name"></td></tr>
		  <tr><td>Surname</td><td><input type="text" name="Surname"></td></tr>
		  <tr><td>Birth Year</td><td><input type="text" name="BirthYear"></td></tr>
		  <tr><td></td><td><input type="submit" value="Update Information"></td>
		  <td><input type=hidden name="NationalID" value="'''+NationalID+'''"></td></tr>	
		  </form></table>'''

	
@route('/update',method='POST')
def update():  		
	NationalID=request.forms.get('NationalID')
	Name=request.forms.get('Name')
	Surname=request.forms.get('Surname')
	BirthYear=request.forms.get('BirthYear')
	

	if((not Name) and (not Surname) and (not BirthYear)):
		return '''<strong>Fields are empty:record stays as it is!</strong><br>
			  <button onclick="goBack()">Go Back</button>
				<script>
				function goBack() {
			    		window.history.back();
				}
				</script>'''

	if((not BirthYear.isdigit()) and BirthYear):
		return '''<strong>Please give an integer for Birth Year!</strong><br>
			  <button onclick="goBack()">Go Back</button>
				<script>
				function goBack() {
			    		window.history.back();
				}
				</script>'''	

	db = pymysql.connect('localhost',port=3306,user='root',passwd='',db='songs',charset='utf8')
	cursor = db.cursor()


	if((not Name) and (not Surname) and BirthYear):	
		sql='''UPDATE kalitexnis
		       SET etos_gen=\''''+BirthYear+'''\'
		       WHERE ar_taut=\''''+NationalID+'''\';'''
	elif((not Name) and Surname and (not BirthYear)):
		sql='''UPDATE kalitexnis
		       SET epitheto=\''''+Surname+'''\'
		       WHERE ar_taut=\''''+NationalID+'''\';'''
	elif((not Name) and Surname and BirthYear):
		sql='''UPDATE kalitexnis
		       SET etos_gen=\''''+BirthYear+'''\',epitheto=\''''+Surname+'''\'
		       WHERE ar_taut=\''''+NationalID+'''\';'''
	elif(Name and (not Surname) and (not BirthYear)):
		sql='''UPDATE kalitexnis
		       SET onoma=\''''+Name+'''\'
		       WHERE ar_taut=\''''+NationalID+'''\';'''
	elif(Name and (not Surname) and BirthYear):
		sql='''UPDATE kalitexnis
		       SET onoma=\''''+Name+'''\',etos_gen=\''''+BirthYear+'''\'
		       WHERE ar_taut=\''''+NationalID+'''\';'''
	elif(Name and Surname and (not BirthYear)):
		sql='''UPDATE kalitexnis
		       SET onoma=\''''+Name+'''\',epitheto=\''''+Surname+'''\'
		       WHERE ar_taut=\''''+NationalID+'''\';'''
	elif(Name and Surname and BirthYear):
		sql='''UPDATE kalitexnis
		       SET onoma=\''''+Name+'''\',epitheto=\''''+Surname+'''\',etos_gen=\''''+BirthYear+'''\'
		       WHERE ar_taut=\''''+NationalID+'''\';'''


	cursor.execute(sql)
	db.commit()

	cursor.close()
	db.close()

	return '''<strong>Artist Updated!</strong><br>
			<button onclick="goBack()">Go Back</button>
			<script>
			function goBack() {
			    window.history.back();
			}
			</script>'''

@route('/Presentation_Of_Songs',method='POST')
def Presentation_Of_Songs():
	return '''
		<h1><strong>Presentation Of Songs</strong>
		</h1><hr><br>

		<table><form name="search" action="/search_song" method="post">
			<tr><td>Song Title:<td><input type="text" name="SongTitle"></td></tr>
			<tr><td>Production Year:<td><input type="text" name="ProductionYear"></td></tr>
			<tr><td>Company:</td><td><input type="text" name="Company"></td></tr>
			<tr><td></td><td><input type="submit" value="Submit"></td></tr>
			</form></table><hr>
		'''

@route('/search_song',method='POST')
def search_song():
	SongTitle=request.forms.get('SongTitle')
	ProductionYear=request.forms.get('ProductionYear')
	Company=request.forms.get('Company')
	
	db = pymysql.connect('localhost',port=3306,user='root',passwd='',db='songs',charset='utf8')
	cursor = db.cursor()

	if(SongTitle and ProductionYear and (not Company)):
		sql='''SELECT t.titlos,t.sinthetis,t.etos_par,t.stixourgos
		       FROM tragoudi t
		       WHERE t.titlos=\"'''+str(SongTitle)+'''\" AND t.etos_par=\"'''+str(ProductionYear)+'''\";'''
	elif(SongTitle and (not ProductionYear) and Company):
		sql='''SELECT t.titlos,t.sinthetis,t.etos_par,t.stixourgos
	       	       FROM tragoudi t,singer_prod sp,cd_production cp
	       	       WHERE t.titlos=\"'''+str(SongTitle)+'''\" AND etaireia=\"'''+str(Company)+'''\" AND t.titlos=sp.title AND sp.cd=cp.code_cd;'''
	elif(SongTitle and (not ProductionYear) and (not Company)):
		sql='''SELECT t.titlos,t.sinthetis,t.etos_par,t.stixourgos
	       	       FROM tragoudi t
	       	       WHERE t.titlos=\"'''+str(SongTitle)+'''\";'''	
	elif((not SongTitle) and ProductionYear and Company):
		sql='''SELECT t.titlos,t.sinthetis,t.etos_par,t.stixourgos
	       	       FROM tragoudi t,singer_prod sp,cd_production cp
	       	       WHERE t.etos_par=\"'''+str(ProductionYear)+'''\" AND etaireia=\"'''+str(Company)+'''\" AND t.titlos=sp.title AND sp.cd=cp.code_cd;'''
	elif((not SongTitle) and ProductionYear and (not Company)):
		sql='''SELECT t.titlos,t.sinthetis,t.etos_par,t.stixourgos
	       	       FROM tragoudi t
	       	       WHERE t.etos_par=\"'''+str(ProductionYear)+'''\";'''
	elif((not SongTitle) and (not ProductionYear) and Company):
		sql='''SELECT t.titlos,t.sinthetis,t.etos_par,t.stixourgos
	       	       FROM tragoudi t,singer_prod sp,cd_production cp
	       	       WHERE etaireia=\"'''+str(Company)+'''\" AND t.titlos=sp.title AND sp.cd=cp.code_cd;'''
	elif(SongTitle and ProductionYear and Company):
		sql='''SELECT t.titlos,t.sinthetis,t.etos_par,t.stixourgos
	       	       FROM tragoudi t,singer_prod sp,cd_production cp
	               WHERE t.titlos=\"'''+str(SongTitle)+'''\" AND (t.etos_par=\"'''+str(ProductionYear)+'''\" AND etaireia=\"'''+str(Company)+'''\") AND t.titlos=sp.title AND sp.cd=cp.code_cd;'''
	else:
		sql='''SELECT t.titlos,t.sinthetis,t.etos_par,t.stixourgos
	       	       FROM tragoudi t;'''		



	cursor.execute(sql)

	s='''<table border=1>
  		<tr>
    		<th><b>Song Title</b></th>
    		<th><b>Composer</b></th>
		<th><b>ProductionYear</b></th>
		<th><b>SongWriter</b></th>
 	</tr>'''
	
	for row in cursor:
				
		s1=unicode('''<tr><form><td>'''+row[0]+'''</td><td>'''+row[1]+'''</td><td>'''+str(row[2])+'''</td><td>'''+row[3]+'''</td></form></tr>''')
		s=s+s1

	s=s+'''</table>'''

	cursor.close()
	db.close()
	
	return s+'''<button onclick="goBack()">Go Back</button>
			<script>
			function goBack() {
			    window.history.back();
			}
			</script>'''

@route('/Insert_Artists',method='POST')
def Insert_Artists():
	return '''
		<h1><strong>Insert Artists</strong>
		</h1><hr><br>

		<table><form name="search" action="/update_artist" method="post">
			<tr><td>National ID:</td><td><input type="text" name="NationalID"></td></tr>
			<tr><td>Name:</td><td><input type="text" name="Name"></td></tr>
			<tr><td>Surname:</td><td><input type="text" name="Surname"></td></tr>
			<tr><td>Birth Year:</td><td><input type="number" name="BirthYear" min=1900 max=2016></td></tr>	
			<tr><td></td><td><input type="submit" value="Update Information"></td><tr>
			</form></table><hr>
		'''

@route('/update_artist',method='POST')
def Update_Artist():
	NationalID=request.forms.get('NationalID')
	Name=request.forms.get('Name')
	Surname=request.forms.get('Surname')
	BirthYear=request.forms.get('BirthYear')

	if (not NationalID):
		return '''<strong>Error!NationalID is missing!All fields must be completed!</strong><br>
				<button onclick="goBack()">Go Back</button>
				<script>
				function goBack() {
			    		window.history.back();
				}
				</script>'''

	
	if (not Name):
		return '''<strong>Error!Name is missing!All fields must be completed!</strong><br>
				<button onclick="goBack()">Go Back</button>
				<script>
				function goBack() {
			    		window.history.back();
				}
				</script>'''

	if (not Surname):
		return '''<strong>Error!Surname is missing!All fields must be completed!</strong><br>
				<button onclick="goBack()">Go Back</button>
				<script>
				function goBack() {
			    		window.history.back();
				}
				</script>'''

	if (not BirthYear):
		return '''<strong>Error!Birth Year is missing!All fields must be completed!</strong><br>
				<button onclick="goBack()">Go Back</button>
				<script>
				function goBack() {
			    		window.history.back();
				}
				</script>'''


	db = pymysql.connect('localhost',port=3306,user='root',passwd='',db='songs',charset='utf8')
	cursor = db.cursor()

	sql='''SELECT *
		FROM kalitexnis
		WHERE ar_taut=\"'''+NationalID+'''\";'''
	
	i=0;

	cursor.execute(sql)

	for row in cursor:
		i=i+1
	
	cursor = db.cursor()

	if i==0:
		sql1='''INSERT INTO kalitexnis (ar_taut,onoma,epitheto,etos_gen)
			VALUES (\''''+NationalID+'''\',\''''+Name+'''\',\''''+Surname+'''\',\''''+str(BirthYear)+'''\');'''
		cursor.execute(sql1)
		db.commit()
	else:
		cursor.close()
		db.close()
		return '''<strong>Error!NationalID already exists!</strong><br>
			  <button onclick="goBack()">Go Back</button>
				<script>
				function goBack() {
			    		window.history.back();
				}
				</script>'''


	cursor.close()
	db.close()
	return '''<strong>Artist Updated!</strong><br>
			<button onclick="goBack()">Go Back</button>
			<script>
			function goBack() {
			    window.history.back();
			}
			</script>'''

@route('/Insert_Songs',method='POST')
def Insert_Songs():
	

	string = '''
		<h1><strong>Insert Songs</strong>
		</h1><hr><br>

		<table><form name="search" action="/update_song" method="post">
			<tr><td>Title:</td><td><input type="text" name="Title"></td></tr>
			<tr><td>Production Year:</td><td><input type="text" name="ProductionYear"></td></tr>
			<tr><td>CD:</td><td><select name="CD">'''
			

					

	db = pymysql.connect('localhost',port=3306,user='root',passwd='',db='songs',charset='utf8')
	cursor = db.cursor()

	sql='''SELECT DISTINCT cd
	       FROM singer_prod;'''

	cursor.execute(sql)

	for row in cursor:
		string = string+'''<option value=\"'''+str(row[0])+'''\">'''+str(row[0])+'''</option>'''

	sql1='''SELECT DISTINCT ar_taut	       
		FROM  kalitexnis;'''

	cursor.execute(sql1)

	string = string+'''</select></td></tr>
				<tr><td>Singer:</td><td><select name="Singer">'''
	for row in cursor:
		string=string+'''<option value=\"'''+str(row[0])+'''\">'''+str(row[0])+'''</option>'''
	
	cursor.execute(sql1)

	string=string+'''</select></td></tr>
			<tr><td>Composer:</td><td><select name="Composer">'''
	for row in cursor:
		string=string+'''<option value=\"'''+str(row[0])+'''\">'''+str(row[0])+'''</option>'''
	
	cursor.execute(sql1)
			
	string=string+'''</select></td></tr>
			<tr><td>SongWriter:</td><td><select name="SongWriter">''' 
	for row in cursor:
		string=string+'''<option value=\"'''+str(row[0])+'''\">'''+str(row[0])+'''</option>'''			
	string=string+'''</select></td></tr>	
				<tr><td></td><td><input type="submit" value="Submit"></td></tr>
			</form></table><hr>
		'''

	cursor.close()
	db.close()

	
	return string

@route('/update_song',method='POST')
def Update_Song():
	Title=request.forms.get('Title')
	ProductionYear=request.forms.get('ProductionYear')
	CD=request.forms.get('CD')
	Singer=request.forms.get('Singer')
	Composer=request.forms.get('Composer')
	SongWriter=request.forms.get('Singer')

	if (not Title):
		return '''<strong>Error!Title is missing!All fields must be completed!</strong><br>
				<button onclick="goBack()">Go Back</button>
				<script>
				function goBack() {
			    		window.history.back();
				}
				</script>'''

	if(not ProductionYear):
		return '''<strong>Error!Production year is missing!All fields must be completed!</strong><br>
				<button onclick="goBack()">Go Back</button>
				<script>
				function goBack() {
			    		window.history.back();
				}
				</script>'''


	if(((not ProductionYear.isdigit()) and ProductionYear)):
		return '''<strong>Error!Production year must be an integer!</strong><br>
				<button onclick="goBack()">Go Back</button>
				<script>
				function goBack() {
			    		window.history.back();
				}
				</script>'''

	if ((not CD) or (not Singer) or (not Composer) or (not SongWriter)):
		return '''<strong>Error!Choose values for CD,Singer,Composer and Song Writer!</strong><br>
				<button onclick="goBack()">Go Back</button>
				<script>
				function goBack() {
			    		window.history.back();
				}
				</script>'''


	db = pymysql.connect('localhost',port=3306,user='root',passwd='',db='songs',charset='utf8')
	cursor = db.cursor()

	sql='''SELECT *
		FROM tragoudi
		WHERE titlos=\"'''+Title+'''\";'''

	cursor.execute(sql)

	i=0;

	for row in cursor:
		i=i+1

	cursor = db.cursor()

	if i==0:
		sql1='''INSERT INTO tragoudi(titlos,sinthetis,etos_par,stixourgos)
			VALUES(\''''+Title+'''\',\''''+Composer+'''\',\''''+ProductionYear+'''\',\''''+SongWriter+'''\');'''
		cursor.execute(sql1)
		db.commit()		

		sql2 = '''SET FOREIGN_KEY_CHECKS=0;
		          INSERT INTO singer_prod(cd,tragoudistis,title) 
			  VALUES (\''''+CD+'''\',\''''+Singer+'''\',\''''+Title+'''\');
			  SET FOREIGN_KEY_CHECKS=1;'''
		
		cursor.execute(sql2)
		db.commit()
	else:
		cursor.close()
		db.close()
		return '''<strong>Error!This song already exists!</strong><br>
			  <button onclick="goBack()">Go Back</button>
				<script>
				function goBack() {
			    		window.history.back();
				}
				</script>'''


	cursor.close()
	db.close()

	return '''<strong>Song Updated!</strong><br>
		  <button onclick="goBack()">Go Back</button>
				<script>
				function goBack() {
			    		window.history.back();
				}
				</script>'''
run(host='localhost',port=9090,debug=True, reloader=True)
