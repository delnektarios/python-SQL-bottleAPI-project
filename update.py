from bottle import get,post,request,run,route,put
import pymysql

@route('/app')
def app():

	return '''<form name="Update and Search Artists" action="/Update_and_Search_Artists" method="post">
		  <input type="submit" value="Update & Search Artists"></form>
		  <form name="Search Songs" action="/Presentation_Of_Songs" method="post">
		  <input type="submit" value="Search Songs"></form>
		  <form name="Insert Artist" action="/Insert_Artists" method="post">
		  <input type="submit" value="Insert Artists"></form>
		  <form name="Insert Song" action="/Insert_Songs" method="post">
		  <input type="submit" value="Insert Songs"></form>'''

@route('/Update_and_Search_Artists',method='POST')
def Update_and_Search_Artists():

	return '''
		
		<h1><strong>Presentation Of Artists</strong>
		</h1><hr><br>

		<table><form name="search" action="/do_search" method="post">
			<tr><td>Name:</td><td><input type="text" name="name"></td></tr>
			<tr><td>Surame:</td><td><input type="text" name="surname"></td></tr>
			<tr><td>Birth Year-From:</td><td><input type="text" name="birthfrom"></td></tr>
			<tr><td>Birth Year-To:</td><td><input type="text" name="birthto"></td></tr>
			<tr><td></td><td><input type="radio" name="type" value="Singer"> Singer</td></tr>
			<tr><td>Type:</td><td><input type="radio" name="type" value="SongWriter"> SongWriter</td></tr>
			<tr><td></td><td><input type="radio" name="type" value="Composer" checked> Composer</td></tr>
			<tr><td></td><td><input type="submit" value="Submit"></td></tr>
			<br></form></table><hr>
		'''

@route('/do_search',method='POST')
def do_search():
	name=request.forms.get('name')
	surname=request.forms.get('surname')
	birthfrom=request.forms.get('birthfrom')
	birthto=request.forms.get('birthto')

	tp=request.forms.get('type')
	sql=''
	
	if tp == 'Singer':
		if((not name) and (not surname) and (not birthfrom) and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif((not name) and (not surname) and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.etos_gen >=\"'''+str(birthfrom)+'''\";'''
		elif((not name) and (not surname) and birthfrom and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND
				 k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif((not name) and surname and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.epitheto=\"'''+surname+'''\";'''
		elif(name and (not surname) and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.onoma=\"'''+name+'''\";'''
		elif((not name) and surname and (not birthfrom) and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.epitheto=\"'''+surname+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif((not name) and surname and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\";'''
		elif((not name) and surname and birthfrom and birthto):	
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\";'''			
		elif(name and (not surname) and (not birthfrom) and birthto):	
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.onoma=\"'''+name+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif(name and (not surname) and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.onoma=\"'''+name+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\";'''
		elif(name and (not surname) and birthfrom and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND (k.onoma=\"'''+name+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\");'''
		elif(name and surname and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\";'''
		elif(name and surname and (not birthfrom) and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND (k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\")'''
		elif(name and surname and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND (k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\");'''
		elif(name and surname and birthfrom and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,singer_prod s
				 WHERE k.ar_taut = s.tragoudistis AND (k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND
				 k.etos_gen <=\"'''+str(birthto)+'''\");'''
		else:
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				     FROM kalitexnis k,singer_prod s
				     WHERE k.ar_taut=s.tragoudistis;'''
	elif tp == 'SongWriter':
		if((not name) and (not surname) and (not birthfrom) and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif((not name) and (not surname) and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.etos_gen >=\"'''+str(birthfrom)+'''\";'''
		elif((not name) and (not surname) and birthfrom and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND
				 k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif((not name) and surname and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.epitheto=\"'''+surname+'''\";'''
		elif(name and (not surname) and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.onoma=\"'''+name+'''\";'''
		elif((not name) and surname and (not birthfrom) and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.epitheto=\"'''+surname+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif((not name) and surname and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\";'''
		elif((not name) and surname and birthfrom and birthto):	
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\";'''			
		elif(name and (not surname) and (not birthfrom) and birthto):	
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.onoma=\"'''+name+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif(name and (not surname) and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.onoma=\"'''+name+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\";'''
		elif(name and (not surname) and birthfrom and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND (k.onoma=\"'''+name+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\");'''
		elif(name and surname and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\";'''
		elif(name and surname and (not birthfrom) and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND (k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\")'''
		elif(name and surname and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND (k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\");'''
		elif(name and surname and birthfrom and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.stixourgos AND (k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND
				 k.etos_gen <=\"'''+str(birthto)+'''\");'''
		else:
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				     FROM kalitexnis k,tragoudi t
				     WHERE k.ar_taut=t.stixourgos;'''
	elif tp == 'Composer':
		if((not name) and (not surname) and (not birthfrom) and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif((not name) and (not surname) and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.etos_gen >=\"'''+str(birthfrom)+'''\";'''
		elif((not name) and (not surname) and birthfrom and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND
				 k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif((not name) and surname and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.epitheto=\"'''+surname+'''\";'''
		elif(name and (not surname) and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.onoma=\"'''+name+'''\";'''
		elif((not name) and surname and (not birthfrom) and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.epitheto=\"'''+surname+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif((not name) and surname and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\";'''
		elif((not name) and surname and birthfrom and birthto):	
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\";'''			
		elif(name and (not surname) and (not birthfrom) and birthto):	
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.onoma=\"'''+name+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\";'''
		elif(name and (not surname) and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.onoma=\"'''+name+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\";'''
		elif(name and (not surname) and birthfrom and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND (k.onoma=\"'''+name+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\");'''
		elif(name and surname and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\";'''
		elif(name and surname and (not birthfrom) and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND (k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\" AND k.etos_gen <=\"'''+str(birthto)+'''\")'''
		elif(name and surname and birthfrom and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND (k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\");'''
		elif(name and surname and birthfrom and birthto):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				 FROM kalitexnis k,tragoudi t
				 WHERE k.ar_taut = t.sinthetis AND (k.onoma=\"'''+name+'''\" AND
				 k.epitheto=\"'''+surname+'''\" AND k.etos_gen >=\"'''+str(birthfrom)+'''\" AND
				 k.etos_gen <=\"'''+str(birthto)+'''\");'''
		elif((not name) and (not surname) and (not birthfrom) and (not birthto)):
			sql =sql+ '''SELECT distinct ar_taut,onoma,epitheto,etos_gen
				     FROM kalitexnis k,tragoudi t
				     WHERE k.ar_taut=t.sinthetis;'''

	
	db = pymysql.connect('localhost',port=3306,user='root',passwd='',db='songs',charset='utf8')
	cursor = db.cursor()
	
	cursor.execute(sql)

	s='''<table border=1>
  		<tr>
    		<th><b>National ID</b></th>
    		<th><b>Name</b></th>
    		<th><b>Surname</b></th>
		<th><b>Birth_Year</b></th>
		<th><b>Edit?</b></th>
 	</tr>'''
	
	for row in cursor:
				
		s1=unicode('''<tr><form name="edit" action="/edit_me" method="post">
				<td>'''+row[0]+'''</td>
				<td>'''+row[1]+'''</td>
				<td>'''+row[2]+'''</td>
				<td>'''+str(row[3])+'''</td>
				<td><input type="submit" value="Edit me!">
				<div>
				<input type=hidden name="NationalID" value="'''+row[0]+'''">
				<input type hidden name="Name" value="'''+row[1]+'''">
				<input type hidden name="Surname" value="'''+row[2]+'''">
				<input type hidden name="Birth_Year" value="'''+str(row[3])+'''"></div></td></form></tr>''')
		s=s+s1

	s=s+'''</table>'''

	cursor.close()
	db.close()
	
	return s

@route('/edit_me',method='POST')
def edit_me():
	
	NationalID=request.forms.get('NationalID')
	Name=request.forms.get('Name')
	Surname=request.forms.get('Surname')
	Birth_Year=request.forms.get('Birth_Year')

	return '''<h1><hr><strong>Update Artist Information</strong></h1><hr>
		  <table><form name="update" action="/update" method="post">
		  <tr><td>Name</td><td><input type="text" name="Name" value="'''+str(Name)+'''"></td></tr>
		  <tr><td>Surname</td><td><input type="text" name="Surname" value="'''+str(Surname)+'''"></td></tr>
		  <tr><td>Birth Year</td><td><input type="text" name="BirthYear" value="'''+str(Birth_Year)+'''"></td></tr>
		  <tr><td></td><td><input type="submit" value="Update Information"></td>
		  <td><input type=hidden name="NationalID" value="'''+NationalID+'''"></td></tr>	
		  </form></table>'''

	
@route('/update',method='POST')
def update():  		
	NationalID=request.forms.get('NationalID')
	Name=request.forms.get('Name')
	Surname=request.forms.get('Surname')
	BirthYear=request.forms.get('BirthYear')
	

	if((not Name) and (not Surname) and (not BirthYear)):
		return '''<strong>Fields are empty:record stays as it is!</strong><br>
			  <button onclick="goBack()">Go Back</button>
				<script>
				function goBack() {
			    		window.history.back();
				}
				</script>'''

	if((not BirthYear.isdigit()) and BirthYear):
		return '''<strong>Please give an integer for Birth Year!</strong><br>
			  <button onclick="goBack()">Go Back</button>
				<script>
				function goBack() {
			    		window.history.back();
				}
				</script>'''	

	db = pymysql.connect('localhost',port=3306,user='root',passwd='',db='songs',charset='utf8')
	cursor = db.cursor()


	if((not Name) and (not Surname) and BirthYear):	
		sql='''UPDATE kalitexnis
		       SET etos_gen=\''''+BirthYear+'''\'
		       WHERE ar_taut=\''''+NationalID+'''\';'''
	elif((not Name) and Surname and (not BirthYear)):
		sql='''UPDATE kalitexnis
		       SET epitheto=\''''+Surname+'''\'
		       WHERE ar_taut=\''''+NationalID+'''\';'''
	elif((not Name) and Surname and BirthYear):
		sql='''UPDATE kalitexnis
		       SET etos_gen=\''''+BirthYear+'''\',epitheto=\''''+Surname+'''\'
		       WHERE ar_taut=\''''+NationalID+'''\';'''
	elif(Name and (not Surname) and (not BirthYear)):
		sql='''UPDATE kalitexnis
		       SET onoma=\''''+Name+'''\'
		       WHERE ar_taut=\''''+NationalID+'''\';'''
	elif(Name and (not Surname) and BirthYear):
		sql='''UPDATE kalitexnis
		       SET onoma=\''''+Name+'''\',etos_gen=\''''+BirthYear+'''\'
		       WHERE ar_taut=\''''+NationalID+'''\';'''
	elif(Name and Surname and (not BirthYear)):
		sql='''UPDATE kalitexnis
		       SET onoma=\''''+Name+'''\',epitheto=\''''+Surname+'''\'
		       WHERE ar_taut=\''''+NationalID+'''\';'''
	elif(Name and Surname and BirthYear):
		sql='''UPDATE kalitexnis
		       SET onoma=\''''+Name+'''\',epitheto=\''''+Surname+'''\',etos_gen=\''''+BirthYear+'''\'
		       WHERE ar_taut=\''''+NationalID+'''\';'''


	cursor.execute(sql)
	db.commit()

	cursor.close()
	db.close()

	return '''<strong>Artist Updated!</strong><br>
			<button onclick="goBack()">Go Back</button>
			<script>
			function goBack() {
			    window.history.back();
			}
			</script>'''

run(host='localhost',port=9090,debug=True, reloader=True)
